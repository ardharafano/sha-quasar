import { createApp } from 'vue'

import { Quasar } from 'quasar'
import '@quasar/extras/material-icons/material-icons.css'

import { createRouter, createWebHistory } from 'vue-router'

import App from './App.vue'
import Home from "./views/Home.vue"
import Kanal from "./views/Kanal.vue"
import './assets/css/style.css'



const routes = [
    { path: "/", component: Home },
    { path: "/kanal", component: Kanal },
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes
});


const myApp = createApp(App).use(router)

myApp.use(Quasar, {
    plugins: {},
})


myApp.mount('#app')